#!/usr/bin/env python
import matplotlib.pyplot as pyplot
import numpy
import pandas
import sklearn.cluster as cluster
from sklearn.cluster import KMeans
from sklearn.manifold import TSNE

from common import describe_data, test_env


def preprocess_data(df):
    # Removing the identifiers
    df = df.drop(df.columns[0], axis=1)

    # Handle the missing data
    df = df.replace(0, numpy.NaN)
    df = df.fillna(df.mean())

    return df


def wcss_plot(df):
    X = df.values

    # Using the elbow method to save clusters
    wcss = []
    max_clusters = 15

    for i in range(1, max_clusters):
        k_means = cluster.KMeans(
            n_clusters=i, init='k-means++', random_state=32)
        k_means.fit(X)
        wcss.append(k_means.inertia_)

    # Plotting the data
    pyplot.plot(range(1, max_clusters), wcss)
    pyplot.title('The Elbow Method')
    pyplot.xlabel('The Number of Clusters')
    pyplot.ylabel('WCSS')
    pyplot.savefig('results/lab4_figure_1.png', papertype='a4')


def plot_clusters(X, y, figure, file):
    # Colors for the plot
    colors = ['tab:blue', 'tab:orange', 'tab:green',
              'tab:red', 'tab:purple', 'tab:brown',
              'tab:pink', 'tab:olive']

    # Markers
    markers = ['o', 'X', 's', 'D']

    color_index = 0
    marker_index = 0

    # Generating the plot
    pyplot.figure(figure)

    len_of_set = len(set(y))
    for i in range(0, len_of_set):
        pyplot.scatter(X[y == i, 0], X[y == i, 1],
                       s=5, c=colors[color_index],
                       marker=markers[marker_index])

        if color_index == (len(colors) - 1):
            color_index = 0
        else:
            color_index += 1

        if marker_index == (len(markers) - 1):
            marker_index = 0
        else:
            marker_index += 1

    pyplot.title(figure)
    pyplot.savefig(file, papertype='a4')
    pyplot.show()


def tsne_visualization(df):
    X = df.values

    # Getting t-SNE
    X_tsne = TSNE(n_components=2, random_state=0).fit_transform(X)

    # Plotting
    plot_clusters(X_tsne,
                  numpy.full(X_tsne.shape[0], 0),
                  't-SNE Without Clusters',
                  'results/cc_tsne_no_clusters.png')

    return X_tsne


def k_means(df):
    k_means = KMeans(n_clusters=15, init='k-means++', random_state=32)
    return k_means.fit_predict(df.values)


def main():
    # 3. Print out Python and available versions.
    modules = ['matplotlib', 'numpy', 'pandas', 'sklearn']
    test_env.versions(modules)

    # 4. Read the dataset file.
    dataset = pandas.read_csv('data/cc_general.csv')
    print('Dataset is read.')

    # 5. Save the dataset description into a file.
    describe_data.print_overview(
        dataset, file='results/dataset_overview.txt')
    describe_data.print_categorical(
        dataset, file='results/dataset_categorical.txt')
    print('Descriptions are generated.')

    # 6. Preprocess the dataset.
    df = preprocess_data(dataset)
    print('Dataset preprocessed.')

    # 7. WCSS plot.
    wcss_plot(df)
    print('WCSS plot saved.')

    # 8. Visualize with t-SNE.
    X = tsne_visualization(df)
    print('t-SNE visualized.')

    # 9. Find k-means.
    y = k_means(df)
    print('K-means calculated.')

    # 10. Visualize with clusters with K-means with different colors.
    plot_clusters(X, y, 'K-means with t-SNE',
                  'results/cc_tsne_8_clusters.png')
    print('Final plot')


if __name__ == '__main__':
    main()
